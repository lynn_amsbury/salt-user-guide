This merge request template is for use with the Feb. 10, 2021 Salt User Guide
documentation jam. For this project, we're converting the Salt User Guide over
to reStructured Text (rST). Read the guidelines for participating at the
[Salt User Guide Doc Jam (Feb 10)](https://docs.google.com/document/d/1Jiabhr9-PGJLZiueuRvaOgELN7nlLc-vYU369q2CN3I/edit?usp=sharing).

If you made it this far, THANK YOU. After submitting this merge request, let
us know that this merge request is complete in one of the following three ways:

- Joining our [Zoom call](https://VMware.zoom.us/j/92385831368?pwd=Q2NJTjhZaFFsK3h1QUJ6WUxncERDZz09)
  and letting us know.
- Linking to your merge request on the [Salt Community Slack workspace](https://saltstackcommunity.herokuapp.com/)
  on the ``#documentation`` channel.
- Sending an email to [docs@saltstack.com](mailto:docs@saltstack.com) with a
  link to your merge request.

Contributors who successfully open a merge request will receive a Salt Project
store credit to buy some swag. If you have submitted a merge request but have
not yet received your store code, let one of the docs jam coordinators know.


## Converted chapter details and related links

- Name of chapter: [write the name of the chapter here]
- Resolves: [link to your salt-user-guide issue here]
- Google Doc chapter: [link to the Google Doc chapter you converted]


## Merge request reviewer checklist

### General checks

- [ ] Verify the pipeline is passing
- [ ] Review new or modified topics to ensure they follow the [style
      guide](https://saltstack.gitlab.io/open/docs/salt-user-guide/topics/style-guide.html)
- [ ] Review new or modified topics to ensure they follow the [Writing Salt
      documentation (rST guide)](https://saltstack.gitlab.io/open/docs/salt-user-guide/topics/writing-salt-docs.html)
- [ ] Ensure all headings use sentence case
- [ ] Ensure the line endings use line breaks for readability
- [ ] Code blocks use the correct highlighting syntax
- [ ] Code blocks use the ``caption`` and ``name`` elements (if needed)
- [ ] Callouts (notes and warning boxes) use the correct syntax

### Compare rendered output to original Google Doc

- [ ] The headings match the weight of the headings in the original Google Doc
- [ ] The images match what is in the original chapter and contain alt text
- [ ] Ensure filenames and in-line commands use ``backticks``
- [ ] Check lists to ensure they are rendered correctly as ordered and unordered
